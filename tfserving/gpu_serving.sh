source $(pwd)/model_path.config
docker stop sim_serving
docker rm sim_serving
docker run --name=sim_serving --runtime=nvidia -t \
-p 8500:8500 \
-p 8501:8501 \
-v "$MODEL:/models/similarity/1" \
-v "$(pwd)/flow.config:/models/model.config" \
-e NVIDIA_VISIBLE_DEVICES=0 \
tensorflow/serving:1.14.0-gpu \
--model_config_file=/models/model.config \
--grpc_channel_arguments=grpc.max_concurrent_streams=1000 \
--per_process_gpu_memory_fraction=0.9 \
--enable_batching=false \
--tensorflow_session_parallelism=4 \
--restart unless-stopped  \
#--batch_timeout_micros=1000 \
#--max_batch_size=1000 \
#--gpu_options.allow_growth=true \
#-e OMP_NUM_THREADS=7 \
#-e TENSORFLOW_INTER_OP_PARALLELISM=2 \
#-e TENSORFLOW_INTRA_OP_PARALLELISM=48 \
