from __future__ import print_function
import os
# This is a placeholder for a Google-internal import.
import cv2
import grpc
import tensorflow as tf
from tensorflow_serving.apis import predict_pb2
from tensorflow_serving.apis import prediction_service_pb2_grpc
from tensorflow_serving.apis import model_pb2
from tensorflow_serving.apis import prediction_log_pb2

import numpy as np

global status
status = {
      1: "UNAVAILABLE - Server connection error",
      2: "INVALID_ARGUMENT - Input name or shape input is wrong",
      3: "FAILED_PRECONDITIO - Signature name is wrong",
      4: "NOT_FOUND - Model name is wrong",
      5: "DEADLINE_EXCEEDED - Time out",
      6: "OUT_NAME - Output name is wrong"
    }
def get_status(status_code):
  global status
  if status_code !=None and status_code <= len(status) and status_code > 0 :
    return status[status_code]
  else:
    return None

class GRPC_inference:
  def __init__(self, hostport, model_name, signature_name, image_shape,
                                          graph_input_name, graph_prediction_name):
    '''
    Defauult image type for inference is RGB
    '''
    #Intit core para
    self.hostport       = hostport
    self.model_name     = model_name
    self.signature_name = signature_name
    self.image_shape    = image_shape
    #Init GRPC para
    channel    = grpc.insecure_channel(hostport)
    self.stub  = prediction_service_pb2_grpc.PredictionServiceStub(channel)
    request    = predict_pb2.PredictRequest()
    request.model_spec.name           = model_name
    request.model_spec.signature_name = signature_name
    self.request                      = request
    #Init graph para
    self.graph_input_name        = graph_input_name
    self.graph_prediction_name   = graph_prediction_name


  def do_inference_sync(self,image, timeout):
    image = cv2.cvtColor(image,cv2.COLOR_BGR2RGB)
    image = cv2.resize(image, self.image_shape, interpolation=cv2.INTER_NEAREST)
    image = np.transpose(image,(2,0,1)).astype('float32')
        
    self.request.inputs[self.graph_input_name].CopyFrom(tf.contrib.util.make_tensor_proto(image,shape=[1]+list(image.shape)))
    result_future = self.stub.Predict.future(self.request,timeout)
    return self.post_processing(result_future)
  

  def post_processing(self,result_future):
    exception = result_future.exception()
    if exception:
      #print the exception below for more details
      #print(exception)
      if exception.code() == grpc.StatusCode.UNAVAILABLE:
        return 1, None
      elif exception.code() == grpc.StatusCode.INVALID_ARGUMENT:
        return 2, None
      elif exception.code() == grpc.StatusCode.FAILED_PRECONDITION:
        return 3, None,
      elif exception.code() == grpc.StatusCode.NOT_FOUND:
        return 4, None
      elif exception.code() == grpc.StatusCode.DEADLINE_EXCEEDED:
        return 5, None
      else:
        return 0, None
    else:
        predict   = np.array(result_future.result().outputs[self.graph_prediction_name].float_val).reshape((2048,))
        return None, predict
       
if __name__ == "__main__":
    HOST_PORT_OD = '192.168.1.102:8500'
    INPUT_SHAPE = (224, 224)
    serving_model =  GRPC_inference(hostport=HOST_PORT_OD, model_name='similarity', signature_name='serving_default', image_shape=INPUT_SHAPE,\
		                                  graph_input_name='image', graph_prediction_name='probability')
    IMAGE_DIR = './images'
    for i,image_name in enumerate(os.listdir(IMAGE_DIR)):
        print(image_name)
        image_raw =  cv2.imread(os.path.join(IMAGE_DIR, image_name))
        h, w, _ = image_raw.shape
        ratio_h, ratio_w = h/INPUT_SHAPE[0], w/INPUT_SHAPE[1]
        image = cv2.cvtColor(image_raw,cv2.COLOR_BGR2RGB)
        status, prediction = serving_model.do_inference_sync(image,10)
        print("Status: ",status)
        print("Predict: ", prediction.shape)
