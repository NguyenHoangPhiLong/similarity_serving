docker stop convert_trt_nv
docker rm convert_trt_nv
docker run -it \
--runtime=nvidia \
-p 8884:8888 \
-v /home/gpu_fti/Desktop/long/similarity_converttrt:/similarity_converttrt \
--workdir /similarity_converttrt \
--name convert_trt_nv \
-e NVIDIA_VISIBLE_DEVICES=0 \
philong/tensortrt:tf-2.2.1
