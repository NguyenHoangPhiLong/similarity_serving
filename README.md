# similarity_serving
#
**Pull docker image convert tensor rt in tf2**

`sudo docker pull philong/tensortrt:tf-2.2.1`

**Clone source code in git lab**

`git clone https://gitlab.com/NguyenHoangPhiLong/similarity_serving.git`

**Start convert trt and build serving model from saved model**

`cd similarity_serving`

`sudo bash run_docker_trt.sh`

**Run file Convert_serving_model.ipynb in container**

**Start serving model**
`sudo docker pull tensorflow/serving:1.14.0-gpu`

edit file model_path.config

`sudo bash gpu_serving.sh`

**Test serving**
`python3 request_serving.py`
